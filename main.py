from combatant import Ranger, Karil, Dharok, Guthans, FrostMage, PyroMage, Warrior from arena import Arena
   # Example usage


tim = Ranger("Tim", 99, 10, 10, 50)
jeff = Karil("Jeff", 99, 50, 40, 5, 10)
kevin = Dharok("Kevin", 99, 40, 30, 10)
zac = Guthans("Zac", 99, 45, 20, 10)
jaina = FrostMage("Jaina", 99, 30, 10, 50)
zezima = PyroMage("Zezima", 99, 30, 10, 50)
jay = Warrior("Jay", 100, 1, 99, 1)

falador = Arena("Falador")
falador.add_combatant(tim)
falador.add_combatant(jeff)
falador.list_combatants()
falador.duel(tim, jeff)
falador.list_combatants()
falador.restore_combatants()
falador.remove_combatant(jeff)
falador.list_combatants()

varrock = Arena("Varrock")
varrock.add_combatant(kevin)
varrock.add_combatant(zac)
varrock.list_combatants()
varrock.duel(kevin, zac)
varrock.list_combatants()

wilderness = Arena("Wilderness")
wilderness.add_combatant(jaina)
wilderness.add_combatant(zezima)
wilderness.duel(jaina, zezima)
wilderness.list_combatants()

lumbridge = Arena("Lumbridge")
lumbridge.add_combatant(jaina)
lumbridge.add_combatant(jay)
lumbridge.add_combatant(tim)
lumbridge.duel(jay, tim)
lumbridge.list_combatants()
