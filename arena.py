from filed import Filed
from combatant import Combatant, FrostMage
class Arena:
    def __init__(self, name):
        self.name = name
        self.combatants = []
        self.field = Field(name).field_type

    def add_combatant(self, combatant):
        if combatant not in self.combatants:
            self.combatants.append(combatant)
            print(f"{combatant.name} was added to {self.name}")

    def remove_combatant(self, combatant):
        if combatant in self.combatants:
            self.combatants.remove(combatant)
            print(f"{combatant.name} was removed from {self.name}")
        else:
            print(f"{combatant.name} cannot be removed as they were not found in the arena")

    def list_combatants(self):
        for combatant in self.combatants:
            print(combatant.details())

    def restore_combatants(self):
        for combatant in self.combatants:
            combatant.reset()
        print("----- RESTING -----")
        self.list_combatants()

    def duel(self, combatant1, combatant2):
        if combatant1 not in self.combatants or combatant2 not in self.combatants:
            print(
                f"{combatant1.name if combatant1 not in self.combatants else combatant2.name} was not found in {self.name}'s list of fighters")
            return

        if combatant1.health <= 0 or combatant2.health <= 0:
            print(f"{combatant1.name if combatant1.health <= 0 else combatant2.name} has no health to battle")
            return

        print(
            f"----- Battle has taken place in {self.name} on the {self.field} between {combatant1.name} and {combatant2.name} -----")

        for round_number in range(1, 11):  # max 10 rounds
            print(f"Round {round_number}")

            if self.field == "Toxic Wasteland":
                combatant1.take_damage(5)
                combatant2.take_damage(5)
            elif self.field == "Healing Meadows":
                combatant1.health = min(combatant1.health + 5, combatant1.max_health)
                combatant2.health = min(combatant2.health + 5, combatant2.max_health)

            damage_to_2 = combatant1.attack()
            if isinstance(combatant2, FrostMage) and combatant2.ice_block:
                combatant2.ice_block = False
                print(f"{combatant2.name} ice block absorbed all the damage!")
            else:
                combatant2.take_damage(damage_to_2)

            if combatant2.health == 0:
                break

            damage_to_1 = combatant2.attack()
            if isinstance(combatant1, FrostMage) and combatant1.ice_block:
                combatant1.ice_block = False
                print(f"{combatant1.name} ice block absorbed all the damage!")
            else:
                combatant1.take_damage(damage_to_1)

            if combatant1.health == 0:
                break

        print("---------- END BATTLE ----------")
