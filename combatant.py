class Combatant:
    def __init__(self, name, max_health, strength, defence):
        self.name = name                  # Initialize the combatant's name
        self.max_health = max_health      # Maximum health points
        self.health = max_health          # Current health equals maximum health initially
        self.strength = strength          # Strength points (attack power)
        self.defence = defence            # Defence points

    def take_damage(self, damage):
        actual_damage = max(damage - self.defence, 0)  # Calculate actual damage considering defence
        if hasattr(self, 'armour_value') and self.armour_value > 0:  # Reduce health by actual damage, ensure it doesn't go below 0
            armour_block = min(self.armour_value, actual_damage)
            actual_damage -= armour_block
            self.armour_value -= armour_block
            print(f"{self.name}'s armour blocked {armour_block} damage")
            if self.armour_value == 0:
                print("Armour shattered!")
        self.health = max(self.health - actual_damage, 0)
        if actual_damage > 0:
            print(f"{self.name} took {actual_damage} damage and has {self.health} health remaining")  # Print damage taken and remaining health
        if self.health == 0:
            print(f"{self.name} has been knocked out!")  # Print knockout message if health reaches 0

    def reset(self):
        self.health = self.max_health  # Reset health to maximum health

    def details(self):
        return f"{self.name} is a {self.__class__.__name__} and has the following stats:\n" \
               f"Health: {self.health}\nStrength: {self.strength}\nDefence: {self.defence}\n"
        # Return detailed information about the combatant

    def attack(self):
        raise NotImplementedError("This method should be overridden in subclasses")
        # Throw an error if this method is called directly, as it should be overridden in subclasses
class Mage(Combatant):
    def __init__(self, name, max_health, strength, defence, mana):
        super().__init__(name, max_health, strength, defence)  # Call the parent class's initialization method
        self.mana = mana                      # Mana points
        self.regen_rate = mana / 4            # Mana regeneration rate per turn

    def reset(self):
        super().reset()                       # Call the parent class's reset method
        self.mana = self.max_health // 4      # Reset mana to one-fourth of maximum health

    def details(self):
        return super().details() + \
               f"Mana: {self.mana}\nMana Regeneration Rate: {self.regen_rate}\n"
        # Return detailed information about the mage, including mana and regeneration rate
class PyroMage(Mage):
    def __init__(self, name, max_health, strength, defence, mana):
        super().__init__(name, max_health, strength, defence, mana)  # Call the parent class's initialization method
        self.flame_boost = 1  # Flame boost coefficient, default is 1

    def cast_spell(self):
        if self.mana >= 40:
            self.mana -= 40
            self.flame_boost += 1
            damage = self.strength * self.flame_boost
            print(f"{self.name} casts SuperHeat!")
        elif self.mana >= 10:  # Check if there's enough mana to cast the spell
            self.mana -= 10  # Calculate spell damage
            damage = self.strength * self.flame_boost + 10
            print(f"{self.name} casts Fire Blast!")
        else:
            damage = 0
            print(f"{self.name} tries to cast a spell but lacks sufficient mana!")
        self.mana += self.regen_rate
        return damage

    def attack(self):
        return self.cast_spell()

class FrostMage(Mage):
    def __init__(self, name, max_health, strength, defence, mana):
        super().__init__(name, max_health, strength, defence, mana)
        self.ice_block = False

    def cast_spell(self):
        if self.mana >= 50:
            self.mana -= 50
            self.ice_block = True
            damage = 0
            print(f"{self.name} casts Ice Block!")
        elif self.mana >= 10:
            self.mana -= 10
            damage = self.strength / 4 + 30
            print(f"{self.name} casts Ice Barrage!")
        else:
            damage = 0
            print(f"{self.name} tries to cast a spell but lacks sufficient mana!")
        self.mana += self.regen_rate
        return damage

    def attack(self):
        return self.cast_spell()

class Ranger(Combatant):
    def __init__(self, name, max_health, strength, defence, range_level):
        super().__init__(name, max_health, strength, defence)
        self.range_level = range_level
        self.arrows = 3

    def attack(self):
        if self.arrows > 0:
            self.arrows -= 1
            print(f"{self.name} attacks for {self.range_level} damage!")
            return self.range_level
        else:
            print(f"{self.name} attacks for {self.strength} damage!")
            return self.strength

    def reset(self):
        super().reset()
        self.arrows = 3

class Warrior(Combatant):
    def __init__(self, name, max_health, strength, defence, armour_value):
        super().__init__(name, max_health, strength, defence)
        self.armour_value = armour_value

    def take_damage(self, damage):
        actual_damage = max(damage - self.defence - self.armour_value, 0)
        if self.armour_value > 0:
            print(f"{self.name}'s armour blocked {self.armour_value} damage")
            if actual_damage < damage:
                print("Armour shattered!")
            self.armour_value = max(self.armour_value - 5, 0)
        super().take_damage(actual_damage)

    def reset(self):
        super().reset()
        self.armour_value = 10

    def attack(self):
        return self.strength

class Dharok(Warrior):
    def attack(self):
        missing_health = self.max_health - self.health
        print(f"The power of Dharok activates adding {missing_health} damage")
        return self.strength + missing_health

class Guthans(Warrior):
    def attack(self):
        healing = self.strength // 5
        self.health = min(self.health + healing, self.max_health)
        print(f"The power of Guthans activates healing {self.name} for {healing}")
        return self.strength

class Karil(Warrior):
    def __init__(self, name, max_health, strength, defence, armour_value, range_level):
        super().__init__(name, max_health, strength, defence, armour_value)
        self.range_level = range_level

    def attack(self):
        print(f"The power of Karil activates adding {self.range_level} damage!")
        return self.strength + self.range_level