import random  # Importing the random module for generating random choices

class Field:
    def __init__(self, name):
        self.name = name  # Initializing the 'name' attribute of the Field object
        self.field_type = self.changeField()    # Initializing the 'field_type' attribute by calling the changeField method

    def changeField(self):
        fields = ['Toxic Wasteland', 'Healing Meadows', 'Castle Walls']  # List of possible field types
        return random.choice(fields)  # Randomly selects and returns one of the field types