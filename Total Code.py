import random  # Importing the random module for generating random choices

class Field:
    def __init__(self, name):
        self.name = name  # Initializing the 'name' attribute of the Field object
        self.field_type = self.changeField()  # Initializing the 'field_type' attribute by calling the changeField method

    def changeField(self):
        fields = ['Toxic Wasteland', 'Healing Meadows', 'Castle Walls']  # List of possible field types
        return random.choice(fields)  # Randomly selects and returns one of the field types

class Combatant:
    def __init__(self, name, max_health, strength, defence):
        self.name = name
        self.max_health = max_health
        self.health = max_health
        self.strength = strength
        self.defence = defence

    def take_damage(self, damage):
        actual_damage = max(damage - self.defence, 0)
        if hasattr(self, 'armour_value') and self.armour_value > 0:
            armour_block = min(self.armour_value, actual_damage)
            actual_damage -= armour_block
            self.armour_value -= armour_block
            print(f"{self.name}'s armour blocked {armour_block} damage")
            if self.armour_value == 0:
                print("Armour shattered!")
        self.health = max(self.health - actual_damage, 0)
        if actual_damage > 0:
            print(f"{self.name} took {actual_damage} damage and has {self.health} health remaining")
        if self.health == 0:
            print(f"{self.name} has been knocked out!")

    def reset(self):
        self.health = self.max_health

    def details(self):
        return f"{self.name} is a {self.__class__.__name__} and has the following stats:\n" \
               f"Health: {self.health}\nStrength: {self.strength}\nDefence: {self.defence}\n"

    def attack(self):
        raise NotImplementedError("This method should be overridden in subclasses")

class Mage(Combatant):
    def __init__(self, name, max_health, strength, defence, mana):
        super().__init__(name, max_health, strength, defence)
        self.mana = mana
        self.regen_rate = mana / 4

    def reset(self):
        super().reset()
        self.mana = self.max_health // 4

    def details(self):
        return super().details() + \
               f"Mana: {self.mana}\nMana Regeneration Rate: {self.regen_rate}\n"

class PyroMage(Mage):
    def __init__(self, name, max_health, strength, defence, mana):
        super().__init__(name, max_health, strength, defence, mana)
        self.flame_boost = 1

    def cast_spell(self):
        if self.mana >= 40:
            self.mana -= 40
            self.flame_boost += 1
            damage = self.strength * self.flame_boost
            print(f"{self.name} casts SuperHeat!")
        elif self.mana >= 10:
            self.mana -= 10
            damage = self.strength * self.flame_boost + 10
            print(f"{self.name} casts Fire Blast!")
        else:
            damage = 0
            print(f"{self.name} tries to cast a spell but lacks sufficient mana!")
        self.mana += self.regen_rate
        return damage

    def attack(self):
        return self.cast_spell()

class FrostMage(Mage):
    def __init__(self, name, max_health, strength, defence, mana):
        super().__init__(name, max_health, strength, defence, mana)
        self.ice_block = False

    def cast_spell(self):
        if self.mana >= 50:
            self.mana -= 50
            self.ice_block = True
            damage = 0
            print(f"{self.name} casts Ice Block!")
        elif self.mana >= 10:
            self.mana -= 10
            damage = self.strength / 4 + 30
            print(f"{self.name} casts Ice Barrage!")
        else:
            damage = 0
            print(f"{self.name} tries to cast a spell but lacks sufficient mana!")
        self.mana += self.regen_rate
        return damage

    def attack(self):
        return self.cast_spell()

class Ranger(Combatant):
    def __init__(self, name, max_health, strength, defence, range_level):
        super().__init__(name, max_health, strength, defence)
        self.range_level = range_level
        self.arrows = 3

    def attack(self):
        if self.arrows > 0:
            self.arrows -= 1
            print(f"{self.name} attacks for {self.range_level} damage!")
            return self.range_level
        else:
            print(f"{self.name} attacks for {self.strength} damage!")
            return self.strength

    def reset(self):
        super().reset()
        self.arrows = 3

class Warrior(Combatant):
    def __init__(self, name, max_health, strength, defence, armour_value):
        super().__init__(name, max_health, strength, defence)
        self.armour_value = armour_value

    def take_damage(self, damage):
        actual_damage = max(damage - self.defence - self.armour_value, 0)
        if self.armour_value > 0:
            print(f"{self.name}'s armour blocked {self.armour_value} damage")
            if actual_damage < damage:
                print("Armour shattered!")
            self.armour_value = max(self.armour_value - 5, 0)
        super().take_damage(actual_damage)

    def reset(self):
        super().reset()
        self.armour_value = 10

    def attack(self):
        return self.strength

class Dharok(Warrior):
    def attack(self):
        missing_health = self.max_health - self.health
        print(f"The power of Dharok activates adding {missing_health} damage")
        return self.strength + missing_health

class Guthans(Warrior):
    def attack(self):
        healing = self.strength // 5
        self.health = min(self.health + healing, self.max_health)
        print(f"The power of Guthans activates healing {self.name} for {healing}")
        return self.strength

class Karil(Warrior):
    def __init__(self, name, max_health, strength, defence, armour_value, range_level):
        super().__init__(name, max_health, strength, defence, armour_value)
        self.range_level = range_level

    def attack(self):
        print(f"The power of Karil activates adding {self.range_level} damage!")
        return self.strength + self.range_level

class Arena:
    def __init__(self, name):
        self.name = name
        self.combatants = []
        self.field = Field(name).field_type

    def add_combatant(self, combatant):
        if combatant not in self.combatants:
            self.combatants.append(combatant)
            print(f"{combatant.name} was added to {self.name}")

    def remove_combatant(self, combatant):
        if combatant in self.combatants:
            self.combatants.remove(combatant)
            print(f"{combatant.name} was removed from {self.name}")
        else:
            print(f"{combatant.name} cannot be removed as they were not found in the arena")

    def list_combatants(self):
        for combatant in self.combatants:
            print(combatant.details())

    def restore_combatants(self):
        for combatant in self.combatants:
            combatant.reset()
        print("----- RESTING -----")
        self.list_combatants()

    def duel(self, combatant1, combatant2):
        if combatant1 not in self.combatants or combatant2 not in self.combatants:
            print(
                f"{combatant1.name if combatant1 not in self.combatants else combatant2.name} was not found in {self.name}'s list of fighters")
            return

        if combatant1.health <= 0 or combatant2.health <= 0:
            print(f"{combatant1.name if combatant1.health <= 0 else combatant2.name} has no health to battle")
            return

        print(
            f"----- Battle has taken place in {self.name} on the {self.field} between {combatant1.name} and {combatant2.name} -----")

        for round_number in range(1, 11):  # max 10 rounds
            print(f"Round {round_number}")

            if self.field == "Toxic Wasteland":
                combatant1.take_damage(5)
                combatant2.take_damage(5)
            elif self.field == "Healing Meadows":
                combatant1.health = min(combatant1.health + 5, combatant1.max_health)
                combatant2.health = min(combatant2.health + 5, combatant2.max_health)

            damage_to_2 = combatant1.attack()
            if isinstance(combatant2, FrostMage) and combatant2.ice_block:
                combatant2.ice_block = False
                print(f"{combatant2.name} ice block absorbed all the damage!")
            else:
                combatant2.take_damage(damage_to_2)

            if combatant2.health == 0:
                break

            damage_to_1 = combatant2.attack()
            if isinstance(combatant1, FrostMage) and combatant1.ice_block:
                combatant1.ice_block = False
                print(f"{combatant1.name} ice block absorbed all the damage!")
            else:
                combatant1.take_damage(damage_to_1)

            if combatant1.health == 0:
                break

        print("---------- END BATTLE ----------")

    # Example usage


tim = Ranger("Tim", 99, 10, 10, 50)
jeff = Karil("Jeff", 99, 50, 40, 5, 10)
kevin = Dharok("Kevin", 99, 40, 30, 10)
zac = Guthans("Zac", 99, 45, 20, 10)
jaina = FrostMage("Jaina", 99, 30, 10, 50)
zezima = PyroMage("Zezima", 99, 30, 10, 50)
jay = Warrior("Jay", 100, 1, 99, 1)

falador = Arena("Falador")
falador.add_combatant(tim)
falador.add_combatant(jeff)
falador.list_combatants()
falador.duel(tim, jeff)
falador.list_combatants()
falador.restore_combatants()
falador.remove_combatant(jeff)
falador.list_combatants()

varrock = Arena("Varrock")
varrock.add_combatant(kevin)
varrock.add_combatant(zac)
varrock.list_combatants()
varrock.duel(kevin, zac)
varrock.list_combatants()

wilderness = Arena("Wilderness")
wilderness.add_combatant(jaina)
wilderness.add_combatant(zezima)
wilderness.duel(jaina, zezima)
wilderness.list_combatants()

lumbridge = Arena("Lumbridge")
lumbridge.add_combatant(jaina)
lumbridge.add_combatant(jay)
lumbridge.add_combatant(tim)
lumbridge.duel(jay, tim)
lumbridge.list_combatants()
